-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema Sarpras_SBSN
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema Sarpras_SBSN
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `Sarpras_SBSN` DEFAULT CHARACTER SET utf8 ;
USE `Sarpras_SBSN` ;

-- -----------------------------------------------------
-- Table `Sarpras_SBSN`.`PTN`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Sarpras_SBSN`.`PTN` (
  `ID_PTN` INT NOT NULL,
  `NAMA_PTN` VARCHAR(45) NOT NULL,
  `ALAMAT_PTN` VARCHAR(45) NOT NULL,
  `DIDIRIKAN_TAHUN` VARCHAR(45) NOT NULL,
  `DINEGERIKAN_TAHUN` VARCHAR(45) NOT NULL,
  `CREATE_TIME` VARCHAR(45) NOT NULL,
  `DELETE_TIME` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`ID_PTN`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Sarpras_SBSN`.`USER`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Sarpras_SBSN`.`USER` (
  `ID_USER` INT NOT NULL,
  `NAMA` VARCHAR(255) NOT NULL,
  `EMAIL` VARCHAR(255) NOT NULL,
  `USER_NAME` VARCHAR(255) NOT NULL,
  `PASSWORD` VARCHAR(255) NOT NULL,
  `ROLE` VARCHAR(45) NOT NULL,
  `CREATE_TIME` DATETIME NOT NULL,
  `UPDATE_TIME` DATETIME NOT NULL,
  `PTN_ID_PTN` INT NOT NULL,
  PRIMARY KEY (`ID_USER`),
  INDEX `fk_USER_PTN1_idx` (`PTN_ID_PTN` ASC) VISIBLE,
  CONSTRAINT `fk_USER_PTN1`
    FOREIGN KEY (`PTN_ID_PTN`)
    REFERENCES `Sarpras_SBSN`.`PTN` (`ID_PTN`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Sarpras_SBSN`.`PROFIL_PTN`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Sarpras_SBSN`.`PROFIL_PTN` (
  `ID_PROFIL_PTN` INT NOT NULL,
  `PTN_ID_PTN` INT NOT NULL,
  `ALAMAT_PTN` VARCHAR(45) NULL,
  `JUMLAH_FAKULTAS` VARCHAR(45) NULL,
  `TERAKREDITASI_INSTITUSI` VARCHAR(45) NULL,
  `JUMLAH_FAKULTAS` VARCHAR(45) NULL,
  `JUMLAH_PROGRAM_STUDI` VARCHAR(45) NULL,
  `JUMLAH_MAHASISWA` VARCHAR(45) NULL,
  `JUMLAH_STAFF` VARCHAR(45) NULL,
  `JUMLAH_LAHAN` VARCHAR(45) NULL,
  `JUMLAH_GEDUNG` VARCHAR(45) NULL,
  `LUAS_TOTAL_RUANG_KULIAH` VARCHAR(45) NULL,
  `LUAS_TOTAL_RUANG_LAB` VARCHAR(45) NULL,
  `LUAS_TOTAL_RUANG_KANTOR` VARCHAR(45) NULL,
  `TOTAL_BANDWITH_INTERNET_KAMPUS` VARCHAR(45) NULL,
  `SUDAHKAH_MEMILIKI_MASTER_PLAN` VARCHAR(45) NULL,
  `JUMLAH_DED_YANG_BELUM_DIBANGUN` VARCHAR(45) NULL,
  `JUMLAH_KDP_KONSTRUKSI_YANG_BELUM_SELESAI` VARCHAR(45) NULL,
  `DATE_CREATE` VARCHAR(45) NULL,
  `DATE_UPDATE` VARCHAR(45) NULL,
  `PRIORITAS_TIGAT` VARCHAR(45) NULL,
  PRIMARY KEY (`ID_PROFIL_PTN`, `PTN_ID_PTN`),
  INDEX `fk_PROFIL_PTN_PTN1_idx` (`PTN_ID_PTN` ASC) VISIBLE,
  CONSTRAINT `fk_PROFIL_PTN_PTN1`
    FOREIGN KEY (`PTN_ID_PTN`)
    REFERENCES `Sarpras_SBSN`.`PTN` (`ID_PTN`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Sarpras_SBSN`.`PENDANAAN_SUMBERLAIN`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Sarpras_SBSN`.`PENDANAAN_SUMBERLAIN` (
  `ID_PENDANAAN_LAIN` INT NOT NULL,
  `PTN_ID_PTN` INT NOT NULL,
  `NILAI_PEROLEHAN` VARCHAR(45) NULL,
  `SUMBER_DANA` VARCHAR(45) NULL,
  `BERAKHIR_TAHUN` VARCHAR(45) NULL,
  `ALOKASI_ESELON_SATU` VARCHAR(45) NULL,
  `CAPAIAN_FISIK` VARCHAR(45) NULL,
  `CAPAIAN_SERAPAN` VARCHAR(45) NULL,
  `TAHUN` VARCHAR(45) NULL,
  `DATE_CREATE` VARCHAR(45) NULL,
  `DATE_UPDATE` VARCHAR(45) NULL,
  PRIMARY KEY (`ID_PENDANAAN_LAIN`, `PTN_ID_PTN`),
  INDEX `fk_BOPN_PTN_PTN1_idx` (`PTN_ID_PTN` ASC) VISIBLE,
  CONSTRAINT `fk_BOPN_PTN_PTN1`
    FOREIGN KEY (`PTN_ID_PTN`)
    REFERENCES `Sarpras_SBSN`.`PTN` (`ID_PTN`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Sarpras_SBSN`.`KERJASAMA`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Sarpras_SBSN`.`KERJASAMA` (
  `ID_KERJASAMA` INT NOT NULL,
  `JUDUL_KERJASAMA` VARCHAR(45) NOT NULL,
  `BENEFIT_KERJASAMA` VARCHAR(45) NOT NULL,
  `KETERANGAN_KERJASAMA` VARCHAR(45) NOT NULL,
  `TAHUN_AWAL_KERJASAMA` VARCHAR(45) NOT NULL,
  `TAHUN_AKHIR_KERJASAMA` VARCHAR(45) NOT NULL,
  `DATE_CREATE` VARCHAR(45) NOT NULL,
  `DATE_UPDATE` VARCHAR(45) NOT NULL,
  `PTN_ID_PTN` INT NOT NULL,
  PRIMARY KEY (`ID_KERJASAMA`, `PTN_ID_PTN`),
  INDEX `fk_KERJASAMA_PTN_PTN1_idx` (`PTN_ID_PTN` ASC) VISIBLE,
  CONSTRAINT `fk_KERJASAMA_PTN_PTN1`
    FOREIGN KEY (`PTN_ID_PTN`)
    REFERENCES `Sarpras_SBSN`.`PTN` (`ID_PTN`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Sarpras_SBSN`.`PENDANAAN`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Sarpras_SBSN`.`PENDANAAN` (
  `ID_PENDANAAN` INT NOT NULL,
  `NAMA_PENDANAAN` VARCHAR(45) NOT NULL,
  `KETERANGAN_PENDANAAN` VARCHAR(45) NOT NULL,
  `SELEKSI_PENDANAAN_DIBUKA` VARCHAR(45) NOT NULL,
  `SELEKSI_PENDANAAN_DITUTUP` VARCHAR(45) NOT NULL,
  `DATE_CREATE` VARCHAR(45) NOT NULL,
  `DATE_UPDATE` VARCHAR(45) NULL,
  PRIMARY KEY (`ID_PENDANAAN`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Sarpras_SBSN`.`BANGUNAN`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Sarpras_SBSN`.`BANGUNAN` (
  `ID_BANGUNAN` INT NOT NULL,
  `PTN_ID_PTN` INT NOT NULL,
  `NAMA_BANGUNAN` VARCHAR(45) NULL,
  `ALAMAT_GEDUNG` VARCHAR(45) NULL,
  `NILAI_USULAN_PENYELESAIAN_PEMBANGUNAN_GEDUNG` VARCHAR(45) NULL,
  `NILAI_USULAN_MEUBLEAIR` VARCHAR(45) NULL,
  `NILAI_USULAN_PERALATAN` VARCHAR(45) NULL,
  `LUAS_BANGUNAN_GEDUNG` VARCHAR(45) NULL,
  `KESESUAIAN_GEDUNG_DENGAN_MASTERPLAN` VARCHAR(45) NULL,
  `NAMA_PEMILIK_LAHAN` VARCHAR(45) NULL,
  `STATUS_KEPEMILIKAN_LAHAN` VARCHAR(45) NULL,
  `TERCATAT_DALAM_SIMAK_BMN` VARCHAR(45) NULL,
  `FILE_KEPEMILIKAN LAHAN` VARCHAR(45) NULL,
  `DOKUMEN_ANALISIS_PUPR` VARCHAR(45) NULL,
  `TAHUN_ANALISIS` VARCHAR(45) NULL,
  `DOKUMEN_IMB` VARCHAR(45) NULL,
  `TAHUN_IMB` VARCHAR(45) NULL,
  `DOKUMEN_IJIN_LINGKUNGAN` VARCHAR(45) NULL,
  `BENTUK_DOKUMEN_IJIN_LINGKUNGAN` VARCHAR(45) NULL,
  `TAHUN_DOKUMEN_IJIN_LINGKUNGAN` VARCHAR(45) NULL,
  `SUDAH_TERCATAT_EPLANNING_SARPRAS` VARCHAR(45) NULL,
  `TERCATAT_DENGAN_NILAI_USULAN` VARCHAR(45) NULL,
  `TAHUN_USULAN_ANGGARAN` VARCHAR(45) NULL,
  `RENCANA_SISTEM_PENGADAAN_MEUBLEAIR` VARCHAR(45) NULL,
  `JUMLAH_UNIT_LOKAL` VARCHAR(45) NULL,
  `JUMLAH_UNIT_IMPOR` VARCHAR(45) NULL,
  `TERCATAT_NILAI_USULAN_DI_EPLANNING` VARCHAR(45) NULL,
  `TAHUN_USULAN_ANGGARAN_EPLANNING` VARCHAR(45) NULL,
  `AKAN_DIKELOLA_OLEH` VARCHAR(45) NULL,
  `DIRENCANAKAN_AKAN_DIGUNAKAN` VARCHAR(45) NULL,
  `LEBIH_DITUJUKAN_UNTUK_MENDUKUNG` VARCHAR(45) NULL,
  `INFORMASI_LAIN` VARCHAR(255) NULL,
  PRIMARY KEY (`ID_BANGUNAN`),
  INDEX `fk_BANGUNAN_PTN1_idx` (`PTN_ID_PTN` ASC) VISIBLE,
  CONSTRAINT `fk_BANGUNAN_PTN1`
    FOREIGN KEY (`PTN_ID_PTN`)
    REFERENCES `Sarpras_SBSN`.`PTN` (`ID_PTN`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Sarpras_SBSN`.`PNPB_BOPTN`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Sarpras_SBSN`.`PNPB_BOPTN` (
  `ID_PNBP_BOPTN` INT NOT NULL,
  `NILAI_PNPB_BOPTN` VARCHAR(45) NOT NULL,
  `NILAI_PNPB_SARPRAS` VARCHAR(45) NOT NULL,
  `SISA_PNPB_BOPTN` VARCHAR(45) NOT NULL,
  `TAHUN` VARCHAR(45) NOT NULL,
  `BNPB_BOPTN` VARCHAR(45) NOT NULL,
  `DATE_CREATE` TIMESTAMP NOT NULL,
  `DATE_UPDATE` TIMESTAMP NULL,
  `PTN_ID_PTN` INT NOT NULL,
  PRIMARY KEY (`ID_PNBP_BOPTN`, `PTN_ID_PTN`),
  INDEX `fk_PNPB_BOPTN_PTN1_idx` (`PTN_ID_PTN` ASC) VISIBLE,
  CONSTRAINT `fk_PNPB_BOPTN_PTN1`
    FOREIGN KEY (`PTN_ID_PTN`)
    REFERENCES `Sarpras_SBSN`.`PTN` (`ID_PTN`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Sarpras_SBSN`.`PRIORITAS_PENDANAAN`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Sarpras_SBSN`.`PRIORITAS_PENDANAAN` (
  `ID_PRIORITAS_PENDANAAN` INT NOT NULL,
  `PENDANAAN_ID_PENDANAAN` INT NOT NULL,
  `BANGUNAN_ID_BANGUNAN` INT NOT NULL,
  `PROFIL_PTN_PTN_ID_PTN` INT NOT NULL,
  `PROFIL_PTN_ID_PROFIL_PTN` INT NOT NULL,
  `PNPB_BOPTN_ID_PNBP_BOPTN` INT NOT NULL,
  `PENDANAAN_SUMBERLAIN_ID_PENDANAAN_LAIN` INT NOT NULL,
  `KERJASAMA_PTN_ID_KERJASAMA` INT NOT NULL,
  `STATUS` VARCHAR(45) NOT NULL,
  `URUTAN_PRIORITAS` VARCHAR(45) NULL,
  `KETERANGAN` VARCHAR(255) NULL,
  `DATE_CREATE` VARCHAR(45) NOT NULL,
  `DATE_UPDATE` VARCHAR(45) NULL,
  PRIMARY KEY (`ID_PRIORITAS_PENDANAAN`),
  INDEX `fk_PRIORITAS_PENDANAAN_PENDANAAN1_idx` (`PENDANAAN_ID_PENDANAAN` ASC) VISIBLE,
  INDEX `fk_PRIORITAS_PENDANAAN_BANGUNAN1_idx` (`BANGUNAN_ID_BANGUNAN` ASC) VISIBLE,
  INDEX `fk_PRIORITAS_PENDANAAN_PROFIL_PTN1_idx` (`PROFIL_PTN_ID_PROFIL_PTN` ASC, `PROFIL_PTN_PTN_ID_PTN` ASC) VISIBLE,
  INDEX `fk_PRIORITAS_PENDANAAN_PNPB_BOPTN1_idx` (`PNPB_BOPTN_ID_PNBP_BOPTN` ASC) VISIBLE,
  INDEX `fk_PRIORITAS_PENDANAAN_PENDANAAN_SUMBERLAIN1_idx` (`PENDANAAN_SUMBERLAIN_ID_PENDANAAN_LAIN` ASC) VISIBLE,
  INDEX `fk_PRIORITAS_PENDANAAN_KERJASAMA_PTN1_idx` (`KERJASAMA_PTN_ID_KERJASAMA` ASC) VISIBLE,
  CONSTRAINT `fk_PRIORITAS_PENDANAAN_PENDANAAN1`
    FOREIGN KEY (`PENDANAAN_ID_PENDANAAN`)
    REFERENCES `Sarpras_SBSN`.`PENDANAAN` (`ID_PENDANAAN`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_PRIORITAS_PENDANAAN_BANGUNAN1`
    FOREIGN KEY (`BANGUNAN_ID_BANGUNAN`)
    REFERENCES `Sarpras_SBSN`.`BANGUNAN` (`ID_BANGUNAN`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_PRIORITAS_PENDANAAN_PROFIL_PTN1`
    FOREIGN KEY (`PROFIL_PTN_ID_PROFIL_PTN` , `PROFIL_PTN_PTN_ID_PTN`)
    REFERENCES `Sarpras_SBSN`.`PROFIL_PTN` (`ID_PROFIL_PTN` , `PTN_ID_PTN`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_PRIORITAS_PENDANAAN_PNPB_BOPTN1`
    FOREIGN KEY (`PNPB_BOPTN_ID_PNBP_BOPTN`)
    REFERENCES `Sarpras_SBSN`.`PNPB_BOPTN` (`ID_PNBP_BOPTN`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_PRIORITAS_PENDANAAN_PENDANAAN_SUMBERLAIN1`
    FOREIGN KEY (`PENDANAAN_SUMBERLAIN_ID_PENDANAAN_LAIN`)
    REFERENCES `Sarpras_SBSN`.`PENDANAAN_SUMBERLAIN` (`ID_PENDANAAN_LAIN`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_PRIORITAS_PENDANAAN_KERJASAMA_PTN1`
    FOREIGN KEY (`KERJASAMA_PTN_ID_KERJASAMA`)
    REFERENCES `Sarpras_SBSN`.`KERJASAMA` (`ID_KERJASAMA`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Sarpras_SBSN`.`DED`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Sarpras_SBSN`.`DED` (
  `ID_DED` INT NOT NULL,
  `BANGUNAN_ID_BANGUNAN` INT NOT NULL,
  `NAMA_PERUSAHAAN` VARCHAR(45) NOT NULL,
  `NAMA_DIREKTUR_PERUSAHAAN` VARCHAR(45) NOT NULL,
  `AKTIVITAS_PERUSAHAAN` VARCHAR(45) NOT NULL,
  `NOMOR_TAHUN_KONTRAK_DIBUAT` VARCHAR(45) NOT NULL,
  `KESEDIAAN_PERENCANA_AWAL_LAKUKAN_REVIEW_PERENCANAAN` VARCHAR(45) NOT NULL,
  `UNTUK_BERAPA_GEDUNG` VARCHAR(45) NOT NULL,
  `DOKUMEN_GAMBAR` VARCHAR(45) NOT NULL,
  `RKS` VARCHAR(45) NOT NULL,
  `NILAI_OWNER_ESTIMATE` VARCHAR(45) NOT NULL,
  `NILAI_BOQ` VARCHAR(45) NOT NULL,
  `NILAI_ENGINEERING_ESTIMATE` VARCHAR(45) NOT NULL,
  `DED_KE` VARCHAR(45) NOT NULL,
  `DATE_CREATE` VARCHAR(45) NOT NULL,
  `DATE_UPDATE` VARCHAR(45) NULL,
  PRIMARY KEY (`ID_DED`),
  INDEX `fk_DED_BANGUNAN1_idx` (`BANGUNAN_ID_BANGUNAN` ASC) VISIBLE,
  CONSTRAINT `fk_DED_BANGUNAN1`
    FOREIGN KEY (`BANGUNAN_ID_BANGUNAN`)
    REFERENCES `Sarpras_SBSN`.`BANGUNAN` (`ID_BANGUNAN`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
