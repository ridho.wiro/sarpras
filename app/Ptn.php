<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ptn extends Model
{
    protected $primaryKey = 'id_ptn';

    public function Profil_ptn()
    {
        return $this->hasMany('app\Profil_ptn');
    }

}
