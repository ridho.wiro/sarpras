<?php

namespace App\Admin\Controllers;

use App\Ptn;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class PtnController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Ptn';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Ptn());

        $grid->column('NAMA_PTN', __('NAMA PTN'));
        $grid->column('ALAMAT_PTN', __('ALAMAT PTN'));
        $grid->column('DIDIRIKAN_TAHUN', __('DIDIRIKAN TAHUN'));
        $grid->column('DINEGERIKAN_TAHUN', __('DINEGERIKAN TAHUN'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Ptn::findOrFail($id));

        $show->field('NAMA_PTN', __('NAMA PTN'));
        $show->field('ALAMAT_PTN', __('ALAMAT PTN'));
        $show->field('DIDIRIKAN_TAHUN', __('DIDIRIKAN TAHUN'));
        $show->field('DINEGERIKAN_TAHUN', __('DINEGERIKAN TAHUN'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Ptn());
        $form->display('id_ptn', 'ID');
        $form->text('NAMA_PTN', __('NAMA PTN'));
        $form->text('ALAMAT_PTN', __('ALAMAT PTN'));
        $form->date('DIDIRIKAN_TAHUN', __('DIDIRIKAN TAHUN'))->default(date('Y-m-d'));
        $form->date('DINEGERIKAN_TAHUN', __('DINEGERIKAN TAHUN'))->default(date('Y-m-d'));

        return $form;
    }
}
