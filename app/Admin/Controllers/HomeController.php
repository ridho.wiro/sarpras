<?php

namespace App\Admin\Controllers;
use Admin; 
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\Dashboard;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use App\Models\Ptn;

class HomeController extends Controller
{
    public function index(Content $content)
    {
        $user = Admin::user();
        $username ="'<h1><center>$user->name</center></h1>'";
        return $content
            ->title('Dashboard')
            ->description('Selamat Datang')
            ->row($username)
            ->row(function (Row $row) {
                if(Admin::user()->isRole('ptn')){
                
                }else{
                    $row->column(4, function (Column $column) {
                        $column->append(Dashboard::environment());
                    });
    
                    $row->column(4, function (Column $column) {
                        $column->append(Dashboard::extensions());
                    });
    
                    $row->column(4, function (Column $column) {
                        $column->append(Dashboard::dependencies());
                    });
                }
            });
    }
}
