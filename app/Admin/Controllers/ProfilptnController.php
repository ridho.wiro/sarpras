<?php

namespace App\Admin\Controllers;

use App\Profil_ptn;
// use Encore\Admin\Controllers\AdminController;
// use Encore\Admin\Form;
// use Encore\Admin\Grid;
// use Encore\Admin\Show;

use App\Http\Controllers\Controller;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Encore\Admin\Form;

class ProfilptnController extends Controller
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Profil Perguruan Tinggi Negeri';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Profil_ptn());



        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function index()
    {
        $id=Admin::user()->PTN_ID_PTN;
        $id_profil_ptn=Profil_ptn::find($id);
        var_dump($id_profil_ptn);

        if(isset($id_profil_ptn)){
            return Admin::content(function (Content $content) use ($id_profil_ptn) {
                $content->header('Post');
                $content->description('Create');
                $form = new Form(new Profil_ptn);
                // Displays the record id
                $form->display('id', 'ID');

                // Add an input box of type text
                $form->text('Alamat PTN', 'ALAMAT_PTN');
                $akreditasi = [
                    'A'  => 'A',
                    'B' => 'B',
                    'C'  => 'C',
                    'Belum Terakreditasi' => 'Belum Terakreditasi'
                ];
                $form->select('Akreditasi Institusi', 'Terakreditasi Institusi')->options($akreditasi);
                $form->number('Jumlah Fakultas', 'JUMLAH_FAKULTAS');
                $form->number('Jumlah Prodi', 'JUMLAH_PROGRAM_STUDI');
                $form->number('Jumlah Mahasiswa', 'JUMLAH_MAHASISWA');
                $form->number('Jumlah Staff', 'JUMLAH_STAFF');
                $form->number('Luas Lahan', 'JUMLAH_LAHAN');
                $form->number('Jumlah Gedung', 'JUMLAH_GEDUNG');
                $form->number('Luas Total Ruang Kuliah', 'LUAS_TOTAL_RUANG_KULIAH');
                $form->number('Luas Total Ruang Kantor', 'LUAS_TOTAL_RUANG_KANTOR');
                $form->number('Luas Total Bandwith Internet Kampus', 'TOTAL_BANDWITH_INTERNET_KAMPUS');
                $form->switch('Apakah Memiliki PTN?', 'SUDAHKAH_MEMILIKI_MASTER_PLAN');
                
                // Display two time column 
                $form->display('created_at', 'Created time');
                $form->display('updated_at', 'Updated time');
                return $form;
            });
        }else{
            return Admin::content(function (Content $content) use ($id_profil_ptn) {
                $content->header('Post');
                $content->description('Detail');
                    $content->body(Admin::show(Profil_ptn::find($id_profil_ptn), function (Show $show) {
                    $show->field('id', 'ID');
                    $show->field('title', 'Title');
                    $show->field('content');
                    $show->field('rate');
                    $show->field('created_at');
                    $show->field('updated_at');
                    $show->field('release_at');
    
                }));
            });
        }
    }

    // /**
    //  * Make a form builder.
    //  *
    //  * @return Form
    //  */
    // protected function form()
    // {
    //     $form = new Form(new Profil_ptn());


    //     return $form;
    // }
}
