<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePtnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ptns', function (Blueprint $table) {
            $table->increments('ID_PTN');
            $table->string('NAMA_PTN',80);
            $table->string('ALAMAT_PTN',255);
            $table->date('DIDIRIKAN_TAHUN');
            $table->date('DINEGERIKAN_TAHUN');
            $table->timestamps(0);	
            $table->softDeletes('DELETED_AT', 0);	
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ptns');
    }
}
