<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePendanaansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pendanaans', function (Blueprint $table) {
            $table->bigIncrements('ID_PENDANAAN');
            $table->string('NAMA_PENDANAAN',45);
            $table->string('KETERANGAN_PENDANAAN',45);
            $table->datetime('SELEKSI_PENDANAAN_DIBUKA');
            $table->datetime('SELEKSI_PENDANAAN_DITUTUP');
            $table->timestamps();
            $table->softDeletes('DELETED_AT', 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pendanaans');
    }
}
