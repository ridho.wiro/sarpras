<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePnpbboptnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pnpbboptns', function (Blueprint $table) {
            $table->bigIncrements('ID_PNBP_BOPTN');
            $table->string('PTN_ID_PTN');
            $table->string('NILAI_PNPB_BOPTN',45);
            $table->string('NILAI_PNPB_SARPRAS',45);
            $table->string('SISA_PNPB_BOPTN',45);
            $table->year('TAHUN');
            $table->string('BNPB_BOPTN',45);
            $table->timestamps();
            $table->softDeletes('DELETED_AT', 0);	
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pnpbboptns');
    }
}
