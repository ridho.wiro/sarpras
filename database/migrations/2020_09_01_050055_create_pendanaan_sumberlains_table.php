<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePendanaanSumberlainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pendanaan_sumberlains', function (Blueprint $table) {
            $table->bigIncrements('ID_PENDANAAN_LAIN',);
            $table->string('PTN_ID_PTN');
            $table->string('NILAI_PEROLEHAN',45)->nullable();
            $table->string('SUMBER_DANA',45)->nullable();
            $table->year('BERAKHIR_TAHUN')->nullable();
            $table->string('ALOKASI_ESELON_SATU',255)->nullable();
            $table->string('CAPAIAN_FISIK',45)->nullable();
            $table->string('CAPAIAN_SERAPAN',45)->nullable();
            $table->year('TAHUN',45)->nullable();
            $table->timestamps();
            $table->softDeletes('DELETED_AT', 0);	
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pendanaan_sumberlains');
    }
}
