<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilPtnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profil_ptns', function (Blueprint $table) {
            $table->bigIncrements('ID_PROFIL_PTN');
            $table->string('PTN_ID_PTN');
            $table->string('ALAMAT_PTN',255)->nullable();
            $table->string('TERAKREDITASI_INSTITUSI',10)->nullable();
            $table->string('JUMLAH_FAKULTAS',10)->nullable();
            $table->string('JUMLAH_PROGRAM_STUDI',10)->nullable();
            $table->string('JUMLAH_MAHASISWA',10)->nullable();
            $table->string('JUMLAH_STAFF',10)->nullable();
            $table->string('JUMLAH_LAHAN',10)->nullable();
            $table->string('JUMLAH_GEDUNG',10)->nullable();
            $table->string('LUAS_TOTAL_RUANG_KULIAH',10)->nullable();
            $table->string('LUAS_TOTAL_RUANG_LAB',10)->nullable();
            $table->string('LUAS_TOTAL_RUANG_KANTOR',10)->nullable();
            $table->string('TOTAL_BANDWITH_INTERNET_KAMPUS',10)->nullable();
            $table->string('SUDAHKAH_MEMILIKI_MASTER_PLAN',10)->nullable();
            $table->string('JUMLAH_DED_YANG_BELUM_DIBANGUN',10)->nullable();
            $table->string('JUMLAH_KDP_KONSTRUKSI_YANG_BELUM_SELESAI',10)->nullable();
            $table->timestamps();	
            $table->softDeletes('DELETED_AT', 0);	
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profil_ptns');
    }
}
