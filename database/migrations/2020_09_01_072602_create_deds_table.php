<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deds', function (Blueprint $table) {
            $table->bigIncrements('ID_DED',);
            $table->string('BANGUNAN_ID_BANGUNAN');
            $table->string('NAMA_PERUSAHAAN',255);
            $table->string('NAMA_DIREKTUR_PERUSAHAAN',255);
            $table->string('AKTIVITAS_PERUSAHAAN',255);
            $table->string('NOMOR_TAHUN_KONTRAK_DIBUAT',255);
            $table->string('KESEDIAAN_PERENCANA_AWAL_LAKUKAN_REVIEW_PERENCANAAN',45);
            $table->string('UNTUK_BERAPA_GEDUNG',255);
            $table->string('DOKUMEN_GAMBAR',255);
            $table->string('RKS',255);
            $table->string('NILAI_OWNER_ESTIMATE',255);
            $table->string('NILAI_BOQ',255);
            $table->string('NILAI_ENGINEERING_ESTIMATE',255);
            $table->integer('DED_KE');
            $table->timestamps();
            $table->softDeletes('DELETED_AT', 0);	
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deds');
    }
}
