<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKerjasamasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kerjasamas', function (Blueprint $table) {
            $table->bigIncrements('ID_KERJASAMA');
            $table->string('PTN_ID_PTN');
            $table->string('JUDUL_KERJASAMA',255);
            $table->string('BENEFIT_KERJASAMA',255);
            $table->string('KETERANGAN_KERJASAMA',255);
            $table->year('TAHUN_AWAL_KERJASAMA');
            $table->year('TAHUN_AKHIR_KERJASAMA');
            $table->timestamps();
            $table->softDeletes('DELETED_AT', 0);	
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kerjasamas');
    }
}
