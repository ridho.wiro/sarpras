<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBangunansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bangunans', function (Blueprint $table) {
            $table->bigIncrements('ID_BANGUNAN');
            $table->string('PTN_ID_PTN');
            $table->string('NAMA_BANGUNAN',255)->nullable();
            $table->string('ALAMAT_GEDUNG',255)->nullable();
            $table->string('NILAI_USULAN_PENYELESAIAN_PEMBANGUNAN_GEDUNG',255)->nullable();
            $table->string('NILAI_USULAN_MEUBLEAIR',255)->nullable();
            $table->string('NILAI_USULAN_PERALATAN',255)->nullable();
            $table->string('LUAS_BANGUNAN_GEDUNG',255)->nullable();
            $table->string('KESESUAIAN_GEDUNG_DENGAN_MASTERPLAN',255)->nullable();
            $table->string('NAMA_PEMILIK_LAHAN',255)->nullable();
            $table->string('STATUS_KEPEMILIKAN_LAHAN',255)->nullable();
            $table->string('TERCATAT_DALAM_SIMAK_BMN',255)->nullable();
            $table->string('FILE_KEPEMILIKAN LAHAN',255)->nullable();
            $table->string('DOKUMEN_ANALISIS_PUPR',255)->nullable();
            $table->year('TAHUN_ANALISIS')->nullable();
            $table->string('DOKUMEN_IMB',255)->nullable();
            $table->string('TAHUN_IMB',255)->nullable();
            $table->string('DOKUMEN_IJIN_LINGKUNGAN',255)->nullable();
            $table->string('BENTUK_DOKUMEN_IJIN_LINGKUNGAN',255)->nullable();
            $table->string('TAHUN_DOKUMEN_IJIN_LINGKUNGAN',255)->nullable();
            $table->string('SUDAH_TERCATAT_EPLANNING_SARPRAS',255)->nullable();
            $table->string('TERCATAT_DENGAN_NILAI_USULAN',255)->nullable();
            $table->year('TAHUN_USULAN_ANGGARAN',255)->nullable();
            $table->string('RENCANA_SISTEM_PENGADAAN_MEUBLEAIR',255)->nullable();
            $table->string('JUMLAH_UNIT_LOKAL',255)->nullable();
            $table->string('JUMLAH_UNIT_IMPOR',255)->nullable();
            $table->string('TERCATAT_NILAI_USULAN_DI_EPLANNING',255)->nullable();
            $table->string('TAHUN_USULAN_ANGGARAN_EPLANNING',255)->nullable();
            $table->string('AKAN_DIKELOLA_OLEH',255)->nullable();
            $table->string('DIRENCANAKAN_AKAN_DIGUNAKAN',255)->nullable();
            $table->string('LEBIH_DITUJUKAN_UNTUK_MENDUKUNG',255)->nullable();
            $table->string('INFORMASI_LAIN')->nullable();
            $table->timestamps();
            $table->softDeletes('DELETED_AT', 0);	
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bangunans');
    }
}
