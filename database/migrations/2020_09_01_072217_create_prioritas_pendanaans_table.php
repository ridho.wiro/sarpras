<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrioritasPendanaansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prioritas_pendanaans', function (Blueprint $table) {
            $table->bigIncrements('ID_PRIORITAS_PENDANAAN');
            $table->string('PENDANAAN_ID_PENDANAAN');
            $table->string('BANGUNAN_ID_BANGUNAN');
            $table->string('PROFIL_PTN_PTN_ID_PTN');
            $table->string('PROFIL_PTN_ID_PROFIL_PTN');
            $table->string('PNPB_BOPTN_ID_PNBP_BOPTN');
            $table->string('PENDANAAN_SUMBERLAIN_ID_PENDANAAN_LAIN');
            $table->string('KERJASAMA_PTN_ID_KERJASAMA');
            $table->string('STATUS',45);
            $table->Integer('URUTAN_PRIORITAS')->nullable();
            $table->string('KETERANGAN')->nullable();
            $table->timestamps();
            $table->softDeletes('DELETED_AT', 0);	
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prioritas_pendanaans');
    }
}
